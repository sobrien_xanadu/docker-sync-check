from prometheus_client import start_http_server, REGISTRY
from os import environ
import check
import time
import sys

if __name__ == "__main__":
    # if environ.get('START_PORT'):
    #     running_port = environ.get('START_PORT')
    #     start_http_server(int(running_port))
    # else:
    #     print("START PORT not supplied")
    #     sys.exit(0)
    start_http_server(int(environ.get('port')))
    REGISTRY.register(check.SyncCheck())
    while True:
        time.sleep(1)