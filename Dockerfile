FROM omahoco1/alpine-java-python
ADD run.py /
ADD check.py /
ADD eddieGhostMonitors.jar /
RUN pip install requests prometheus_client cassandra-driver
CMD [ "python", "./run.py" ]
