from subprocess import Popen, PIPE, STDOUT
from prometheus_client import Metric
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from datetime import datetime


def get_edges():
    aws_edge_list = []
    c5_edge_list = []
    auth = PlainTextAuthProvider(username='engineering', password='engineering')
    cas = Cluster(['casdb07dr.aws.xcl.ie'], port = 9042, auth_provider=auth)
    session = cas.connect()
    edge_query = "select id, host from matchbook_federator.nodes where node_type = \'EDGE\' and status = \'ACTIVE\' allow filtering;"
    rows = session.execute(edge_query)
    for edge in rows:
        if "prodaws" in edge.host:
            aws_edge_list.append("http://" + edge.host + ".awsl.xcl.ie:8080")
        if "prod" in edge.host and "prodaws" not in edge.host and "99" not in edge.host:
            c5_edge_list.append("http://" + edge.host + ".c5.xcl.ie:8080")
    aws_edge_str = ' '.join(aws_edge_list)
    c5_edge_str = ' '.join(c5_edge_list)
    return aws_edge_str, c5_edge_str



def run_check(edge_list):
    # aws_edge_list, c5_edge_list = get_edges()
    p = Popen(['java', '-cp', './eddieGhostMonitors.jar',
               'com.matchbook.eddie.operations.monitors.ghostpricecheck.GhostPricesCheck', '-edges',
               edge_list, '-syncCheck'],
              stdout=PIPE, stderr=STDOUT)
    error_count = 0
    events_count = 0
    runners_count = 0
    prices_count = 0
    for line in p.stdout:
        tmp_line = line.split()
        if "e_r_r cnt" in line:
            error_count = tmp_line[-1]
        if "e_r_r events" in line:
            events_count = tmp_line[-1]
        if "e_r_r runners" in line:
            runners_count = tmp_line[-1]
        if "e_r_r prices" in line:
            prices_count = tmp_line[-1]
    return float(error_count), float(events_count), float(runners_count), float(prices_count)


class SyncCheck:
    s = None
    def __init__(self):
        self.s = None

    def collect(self):
        aws_edges, c5_edges = get_edges()
        aws_mrkt, aws_evnt, aws_runn, aws_pric = run_check(aws_edges)
        c5_mrkt, c5_evnt, c5_runn, c5_pric = run_check(c5_edges)
        error_count = Metric('sync_check_errors', 'Error count from sync_check', 'summary')
        error_count.add_sample('sync_check_error_count', value=aws_mrkt, labels={"type": "aws"})
        error_count.add_sample('sync_check_error_count', value=c5_mrkt, labels={"type": "c5"})
        e_count = Metric('sync_check_event', 'Error count from sync_check', 'summary')
        e_count.add_sample('sync_check_event_count', value=aws_evnt, labels={"type": "aws"})
        e_count.add_sample('sync_check_event_count', value=c5_evnt, labels={"type": "c5"})
        r_count = Metric('sync_check_runner', 'Error count from sync_check', 'summary')
        r_count.add_sample('sync_check_runner_count', value=aws_runn, labels={"type": "aws"})
        r_count.add_sample('sync_check_runner_count', value=c5_runn, labels={"type": "c5"})
        p_count = Metric('sync_check_price', 'Error count from sync_check', 'summary')
        p_count.add_sample('sync_check_price_count', value=aws_pric, labels={"type": "aws"})
        p_count.add_sample('sync_check_price_count', value=c5_pric, labels={"type": "c5"})
        yield error_count
        yield e_count
        yield r_count
        yield p_count
